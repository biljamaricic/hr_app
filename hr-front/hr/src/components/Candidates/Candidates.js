import React from "react";
import Axios from "../apis/Axios";
import {withNavigation} from '../../routeconf';
import { Table, Button, Row, Col } from "react-bootstrap";

class Candidates extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
           candidates: []
        }
    }

    componentDidMount() {
        this.getCandidates();
    }

    async getCandidates() {
        try{
            let result = await Axios.get("/candidate");
            this.setState({candidates: result.data});
        }
        catch(error){
            alert("Error occured");
            console.log(error);
        }
    }

    // getCandidates() {


    //     Axios.get('/candidate')
    //         .then(res => {
    //              console.log(res);
    //              this.setState({candidates: res.data});
    //         })
    //         .catch(error => {
    //             console.log(error);
    //             alert('Error occured please try again!');
    //         });
    // }

    renderCandidates() {
        return this.state.candidates.map((candidate) => {
            return (
               <tr key={candidate.id}>
                  <td>{candidate.name}</td>
                  <td>{candidate.dateOfBirth}</td>
                  <td>{candidate.number}</td>
                  <td>{candidate.email}</td>
  
               </tr>
            )
         })
    }


    render() {
        return <>
           <Col> 
                <Row><h1>Candidates</h1></Row>

                <Row>

                    <Table striped hover style={{ marginTop: 5 }}>
                        <thead style={{backgroundColor: "black", color: "white"}}>
                            <tr>
                                <th>Name</th>
                                <th>Date of birth</th>
                                <th>Contact number</th>
                                <th>E-mail</th>
                                <th>Skills</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderCandidates()}
                        </tbody>
                    </Table>
                </Row>
            </Col>
        </>
    }

   
}

export default withNavigation(Candidates);

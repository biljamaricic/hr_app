import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter as Router, Link, Route, Routes} from 'react-router-dom';
import {Container, Navbar, NavbarBrand, Nav, NavLink} from 'react-bootstrap';
//import Home from './components/Home';
import NotFound from './components/NotFound';
import Home from './components/Home';
import Candidates from './components/Candidates/Candidates';

class App extends React.Component {
  render() {
    return(<>
      <Router>
        <Navbar expand bg="dark" variant="dark">
          <NavbarBrand as={Link} to = "/">
            HR
          </NavbarBrand>
          <Nav>
            <NavLink as={Link} to="/candidate">
              Candidates
            </NavLink>
          </Nav>
        </Navbar>
        <Container style={{paddingTop:"10px"}}>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="*" element={<NotFound/>}/>
            <Route path="/candidate" element={<Candidates/>}/>
          </Routes>

        </Container>
      </Router>
    </>)
  }
}

ReactDOM.render(
  <App/>,
  document.querySelector('#root')
);



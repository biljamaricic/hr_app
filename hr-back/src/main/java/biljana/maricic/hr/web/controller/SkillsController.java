package biljana.maricic.hr.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import biljana.maricic.hr.model.Candidate;
import biljana.maricic.hr.model.Skills;
import biljana.maricic.hr.repository.SkillsRepository;
import biljana.maricic.hr.servise.SkillService;
import biljana.maricic.hr.support.SkillsDtoToSkills;
import biljana.maricic.hr.support.SkillsToSkillsDto;
import biljana.maricic.hr.web.dto.CandidateDTO;
import biljana.maricic.hr.web.dto.SkillsDTO;

@RestController
@RequestMapping(value = "/api/skills", produces = MediaType.APPLICATION_JSON_VALUE)
public class SkillsController {

	@Autowired
	private SkillsToSkillsDto toDto;
	
	@Autowired
	private SkillsDtoToSkills fromDto;
	
	@Autowired
	private SkillService service;
	
	@GetMapping
	public ResponseEntity<List<SkillsDTO>> getAll() {
		List<Skills> res = service.findAll();
		
		return new ResponseEntity<>(toDto.convert(res), HttpStatus.OK);
	}
	
	@GetMapping("/search")
	public ResponseEntity<List<SkillsDTO>> search(
			@RequestParam(value = "name", required = true) String name
			){
		
		List<Skills> list = new ArrayList<>();
		
		if(name != "") {
			list = service.search(name);
		}
		
		return new ResponseEntity<>(toDto.convert(list), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<SkillsDTO> getOne(@PathVariable Long id) {
		Skills skill = service.findOne(id);
		
		if(skill != null) {
			return new ResponseEntity<>(toDto.convert(skill), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<SkillsDTO> create(@RequestBody SkillsDTO dto) {
		Skills skill = fromDto.convert(dto);
		Skills ret = service.save(skill);
		
		return new ResponseEntity<>(toDto.convert(ret), HttpStatus.CREATED);
	}
	
	@PutMapping(value="/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<SkillsDTO> update(@PathVariable Long id, @RequestBody SkillsDTO dto) {
		
		if(!id.equals(dto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Skills skill = fromDto.convert(dto);
		Skills ret = service.update(skill);
		
		return new ResponseEntity<>(toDto.convert(ret), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		
		Skills del = service.delete(id);
		
		if(del != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
}

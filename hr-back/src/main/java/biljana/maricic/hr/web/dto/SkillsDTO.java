package biljana.maricic.hr.web.dto;

public class SkillsDTO {

	private Long id;
	
	private String name;
	
	private Long candidateId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCandidateId() {
		return candidateId;
	}

	public void setCandidatId(Long candidatId) {
		this.candidateId = candidatId;
	}
	
	
	
}

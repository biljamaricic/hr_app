package biljana.maricic.hr.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import biljana.maricic.hr.model.Candidate;
import biljana.maricic.hr.servise.CandidateService;
import biljana.maricic.hr.support.CandidateDtoToCandidate;
import biljana.maricic.hr.support.CandidateToCandidateDto;
import biljana.maricic.hr.web.dto.CandidateDTO;

@RestController
@RequestMapping(value = "/api/candidate", produces = MediaType.APPLICATION_JSON_VALUE)
public class CandidateController {
	
	@Autowired
	private CandidateToCandidateDto toDto;
	
	@Autowired
	private CandidateDtoToCandidate fromDto;
	
	@Autowired
	private CandidateService service;
	
	@GetMapping
	public ResponseEntity<List<CandidateDTO>> getAll() {
		List<Candidate> res = service.findAll();
		
		return new ResponseEntity<>(toDto.convert(res), HttpStatus.OK);
	}
	
	@GetMapping("/search")
	public ResponseEntity<List<CandidateDTO>> search(
			@RequestParam(value = "name", required = true) String name
			){
		
		List<Candidate> list = new ArrayList<>();
		
		if(name != "") {
			list = service.search(name);
		}
		
		return new ResponseEntity<>(toDto.convert(list), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<CandidateDTO> getOne(@PathVariable Long id) {
		Candidate candidate = service.findOne(id);
		
		if(candidate != null) {
			return new ResponseEntity<>(toDto.convert(candidate), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CandidateDTO> create(@RequestBody CandidateDTO dto) {
		Candidate candidate = fromDto.convert(dto);
		Candidate ret = service.save(candidate);
		
		return new ResponseEntity<>(toDto.convert(ret), HttpStatus.CREATED);
	}
	
	@PutMapping(value="/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CandidateDTO> update(@PathVariable Long id, @RequestBody CandidateDTO dto) {
		
		if(!id.equals(dto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Candidate candidate = fromDto.convert(dto);
		Candidate ret = service.update(candidate);
		
		return new ResponseEntity<>(toDto.convert(ret), HttpStatus.OK);
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		
		Candidate del = service.delete(id);
		
		if(del != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}
	
}

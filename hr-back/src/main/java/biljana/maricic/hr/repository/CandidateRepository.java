package biljana.maricic.hr.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import biljana.maricic.hr.model.Candidate;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long> {

	Candidate findOneById(Long id);
	List<Candidate> findByNameContains(String name);
	
	
}

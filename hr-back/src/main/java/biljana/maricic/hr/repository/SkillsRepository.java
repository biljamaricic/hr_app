package biljana.maricic.hr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import biljana.maricic.hr.model.Skills;

@Repository
public interface SkillsRepository extends JpaRepository<Skills, Long> {
	
	Skills findOneById(Long id);
	List<Skills> findByNameContains(String name);

}

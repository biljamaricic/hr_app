package biljana.maricic.hr.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import biljana.maricic.hr.model.Skills;
import biljana.maricic.hr.web.dto.SkillsDTO;

@Component
public class SkillsToSkillsDto implements Converter<Skills, SkillsDTO> {

	@Override
	public SkillsDTO convert(Skills source) {
		SkillsDTO dto = new SkillsDTO();
		
		dto.setId(source.getId());
		dto.setName(source.getName());
		dto.setCandidatId(source.getCandidate().getId());
		
		return dto;
	}
	
	public List<SkillsDTO> convert(List<Skills> list) {
		List<SkillsDTO> dto = new ArrayList<SkillsDTO>();
		
		
		for(Skills s : list) {
			dto.add(convert(s));
		}
		
		return dto;
	}

}

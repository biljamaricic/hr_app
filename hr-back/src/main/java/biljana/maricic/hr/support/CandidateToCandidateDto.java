package biljana.maricic.hr.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import biljana.maricic.hr.model.Candidate;
import biljana.maricic.hr.web.dto.CandidateDTO;

@Component
public class CandidateToCandidateDto implements Converter<Candidate, CandidateDTO>{

	@Override
	public CandidateDTO convert(Candidate source) {
		CandidateDTO dto = new CandidateDTO();
		
		dto.setId(source.getId());
		dto.setName(source.getName());
		dto.setDateOfBirth(source.getDateOfBirth());
		dto.setContactNumber(source.getContactNumber());
		dto.setEmail(source.getEmail());
		return dto;
	}
	
	public List<CandidateDTO> convert(List<Candidate> list) {
		List<CandidateDTO> dto = new ArrayList<CandidateDTO>();
		
		for(Candidate c : list) {
			dto.add(convert(c));
		}
		
		return dto;
	}
	
	

}

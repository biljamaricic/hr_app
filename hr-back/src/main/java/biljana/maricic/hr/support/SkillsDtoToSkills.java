package biljana.maricic.hr.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import biljana.maricic.hr.model.Candidate;
import biljana.maricic.hr.model.Skills;
import biljana.maricic.hr.servise.CandidateService;
import biljana.maricic.hr.servise.SkillService;
import biljana.maricic.hr.web.dto.SkillsDTO;

@Component
public class SkillsDtoToSkills implements Converter<SkillsDTO, Skills> {

	@Autowired
	private SkillService skillsService;
	
	@Autowired
	private CandidateService candidateService;
	
	@Override
	public Skills convert(SkillsDTO source) {
		
		Skills skill = skillsService.findOne(source.getId());
		
		if(skill == null) {
			skill = new Skills();
		}
		
		skill.setName(source.getName());
		
		Candidate candidate = candidateService.findOne(source.getCandidateId());
		skill.setCandidate(candidate);
		
		return skill;
	}

	
}

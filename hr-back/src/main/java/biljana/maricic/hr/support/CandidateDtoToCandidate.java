package biljana.maricic.hr.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import biljana.maricic.hr.model.Candidate;
import biljana.maricic.hr.servise.CandidateService;
import biljana.maricic.hr.web.dto.CandidateDTO;

@Component
public class CandidateDtoToCandidate implements Converter<CandidateDTO, Candidate> {

	@Autowired
	private CandidateService candidateService;
	
	@Override
	public Candidate convert(CandidateDTO source) {
		
		Candidate candidate = candidateService.findOne(source.getId());
		
		if(candidate == null) {
			candidate = new Candidate();	
		}
		
		candidate.setName(source.getName());
		candidate.setDateOfBirth(source.getDateOfBirth());
		candidate.setContactNumber(source.getContactNumber());
		candidate.setEmail(source.getEmail());
			
		return candidate;
	}

	
}

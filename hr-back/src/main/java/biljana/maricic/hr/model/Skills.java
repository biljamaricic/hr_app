package biljana.maricic.hr.model;

//import java.util.ArrayList;
//import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "skills")
public class Skills {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String name;
	
	
// Sve zakomentarisano je prilagodjeno za pokusaj ManyToMany veze
//	@ManyToMany(mappedBy = "skills")
//	private List<Candidate> candidates = new ArrayList<>();

	@ManyToOne
	private Candidate candidate;
	
	public Skills() {
		super();
	}
	
	

	public Skills(Long id, String name, Candidate candidate) {
		super();
		this.id = id;
		this.name = name;
		this.candidate = candidate;
	}


//	Prilagodjeno za ManyToMany vezu
//	public Skills(Long id, String name, List<Candidate> candidates) {
//		super();
//		this.id = id;
//		this.name = name;
//		this.candidates = candidates;
//	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}
	
//	Prilagodjeno za ManyToMany vezu
//	public List<Candidate> getCandidates() {
//		return candidates;
//	}
//
//	public void setCandidates(List<Candidate> candidates) {
//		this.candidates = candidates;
//	}
	
	
}

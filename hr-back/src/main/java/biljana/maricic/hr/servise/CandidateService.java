package biljana.maricic.hr.servise;


import java.util.List;

import org.springframework.data.domain.Page;

import biljana.maricic.hr.model.Candidate;
import biljana.maricic.hr.web.dto.CandidateDTO;

public interface CandidateService {

	Candidate findOne(Long id);
	List<Candidate> findAll();
	List<Candidate> search(String name);
	Candidate save(Candidate candidate);
	Candidate update(Candidate candidate);
	Candidate delete(Long id);

}

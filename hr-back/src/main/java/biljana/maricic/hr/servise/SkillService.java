package biljana.maricic.hr.servise;


import java.util.List;

import biljana.maricic.hr.model.Skills;

public interface SkillService {

	Skills findOne(Long id);
	List<Skills> findAll();
	List<Skills> search(String name);
	Skills save(Skills skill);
	Skills update(Skills skill);
	Skills delete(Long id);
	
}

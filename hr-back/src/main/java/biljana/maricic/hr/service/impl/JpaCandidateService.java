package biljana.maricic.hr.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import biljana.maricic.hr.model.Candidate;
import biljana.maricic.hr.repository.CandidateRepository;
import biljana.maricic.hr.servise.CandidateService;
import biljana.maricic.hr.web.dto.CandidateDTO;

@Service
public class JpaCandidateService implements CandidateService{

	@Autowired
	private CandidateRepository candidateRepository;
	
	
	@Override
	public Candidate findOne(Long id) {
		return candidateRepository.findOneById(id);
	}
	
	@Override
	public List<Candidate> findAll() {
		return candidateRepository.findAll();
	}

	@Override
	public Candidate save(Candidate candidate) {
		return candidateRepository.save(candidate);
	}

	@Override
	public Candidate update(Candidate candidate) {
		return candidateRepository.save(candidate);
	}

	
	@Override
	public Candidate delete(Long id) {
		Optional<Candidate> opt = candidateRepository.findById(id);
		if(opt.isPresent()) {
			candidateRepository.deleteById(id);
			return opt.get();
		}
		return null;
	}

	@Override
	public List<Candidate> search(String name) {
		return candidateRepository.findByNameContains(name);
	}
	
}

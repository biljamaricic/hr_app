package biljana.maricic.hr.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import biljana.maricic.hr.model.Skills;
import biljana.maricic.hr.repository.SkillsRepository;
import biljana.maricic.hr.servise.SkillService;

@Service
public class JpaSkillsService implements SkillService{

	@Autowired
	private SkillsRepository skillsRepository;
	
	@Override
	public Skills findOne(Long id) {
		return skillsRepository.findOneById(id);
	}
	
	@Override
	public List<Skills> findAll() {
		return skillsRepository.findAll();
	}

	@Override
	public Skills save(Skills skill) {
		return skillsRepository.save(skill);
	}

	@Override
	public Skills update(Skills skill) {
		return skillsRepository.save(skill);
	}

	@Override
	public Skills delete(Long id) {
		Optional<Skills> opt = skillsRepository.findById(id);
		if(opt.isPresent()) {
			skillsRepository.deleteById(id);
			return opt.get();
		}
		
		return null;
	}

	@Override
	public List<Skills> search(String name) {
		return skillsRepository.findByNameContains(name);
	}

}

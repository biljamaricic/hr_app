INSERT INTO `hr`.`candidate` (`id`, `name`, `birth`, `number`, `email`) VALUES ('1', 'Marko Markovic', '1980-03-13', '063111111', 'aaa@gmail.com');
INSERT INTO `hr`.`candidate` (`id`, `name`, `birth`, `number`, `email`) VALUES ('2', 'Petar Petrovic', '1984-06-11', '063222222', 'bbb@gmail.com');
INSERT INTO `hr`.`candidate` (`id`, `name`, `birth`, `number`, `email`) VALUES ('3', 'Jovana Jovanovic', '1983-09-04', '06333333', 'ccc@gmail.com');
INSERT INTO `hr`.`candidate` (`id`, `name`, `birth`, `number`, `email`) VALUES ('4', 'Ivana Ivanic', '1986-01-20', '063444444', 'ddd@gmail.com');

INSERT INTO `hr`.`skills` (`id`, `name`, `candidate_id`) VALUES ('1', 'Java programing', '2');
INSERT INTO `hr`.`skills` (`id`, `name`, `candidate_id`) VALUES ('2', 'React', '1');
INSERT INTO `hr`.`skills` (`id`, `name`, `candidate_id`) VALUES ('3', 'JavaScript', '4');
INSERT INTO `hr`.`skills` (`id`, `name`, `candidate_id`) VALUES ('4', 'German', '3');

